var app = require('express')();
var http = require('http').Server(app);

var io = require('socket.io')(http);

app.get('/', function(req , res) {
  res.sendFile(__dirname + '/index.html');
});


io.on('connection', function(socket){
  console.log('someone connected to socket.io');

  socket.on('switch', function (msg) {
    socket.broadcast.emit('switch', msg);
    console.log(msg);
  })

  socket.on('switched', function (msg) {
    socket.broadcast.emit('switched', msg);
    console.log(msg);
  })

});

// Aprire due browser a questi due indirizzi
// http://localhost:3000/#table
// http://localhost:3000/#wall

http.listen(3000, function(){
  console.log('listening on *:3000');
});


